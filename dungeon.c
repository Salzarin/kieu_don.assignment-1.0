#include "dijkstras.h"






typedef struct roomdata{
	//X and Y are position of top left of the room.
	int x; 
	int y;
	int w;
	int h;
	int cx;
	int cy;
	bool valid;
} room;



bool checkRoomIntersection(room A, room B){

int Aw = A.w;
int Ah = A.h;
int Bw = B.w;
int Bh = B.h;

//Add one to the bounds, so the bounding box
//guarantees that we have 1 space between rooms
int Aleft = A.x -3;
int Atop = A.y-3;
int Aright = Aleft+Aw+3; 
int Abot = A.y+Ah+3;

int Bleft = B.x-3;
int Btop = B.y-3;
int Bright = Bleft+Bw+3; 
int Bbot = B.y+Bh+3;

	//Bound Check, if any one of these are true,
	// no overlap can exist.
	if(Aright < Bleft) return false;
	if(Aleft > Bright) return false;
	if(Atop > Bbot) return false;
	if(Abot < Btop) return false;
	
				
	//We're returning True if it overlaps
	return true;
}


int main(int argc,char* argv[]){


int Max_Rooms = 5;

if(argc > 1){

	Max_Rooms = 5 < atoi(argv[1]) ? atoi(argv[1]) : 5;
}


//Initialize Dungeon Map
char dungeonmap[MAX_H][MAX_V];

memset(dungeonmap,0,sizeof(dungeonmap));

int rockhardnessmap[MAX_H][MAX_V];

memset(rockhardnessmap,0,sizeof(rockhardnessmap));

//Initialize Time for Rand
time_t ti;

//Seed the RNG
srand((unsigned) time(&ti));

int i, j;
//Initialize the Hardness Map. For now make it random hardness.
for(j = 0;j<MAX_V;j++){
	for(i=0;i<MAX_H;i++){
		rockhardnessmap[i][j] = rand() % 10; 
	}
}



//Make Bounds
for(i = 0; i<MAX_V; i++){
	dungeonmap[0][i] = '|';
	dungeonmap[MAX_H-1][i] = '|';
	rockhardnessmap[0][i] = 1000000; 
	rockhardnessmap[MAX_H-1][i] = 1000000; 
}

for(i = 0; i<MAX_H; i++){
	dungeonmap[i][0] = '-';
	dungeonmap[i][MAX_V-1] = '-';
	rockhardnessmap[i][0] = 1000000; 
	rockhardnessmap[i][MAX_V-1] = 1000000; 
}

//Debug Check for Hardness Map
/*
for(j = 0;j<MAX_V;j++){
	for(i=0;i<MAX_H;i++){
		if(rockhardnessmap[i][j] < 10){
		putchar(48+rockhardnessmap[i][j]);
		
		}
		else{
			putchar('#');
		}	
	}
	putchar('\n');
}
*/


//Create Room Positions
room Room[5];
//Bound Width is equal to the Max Bounds
//subtracted by the left and right wall 
//and a buffer of 2 offsets.
//The reasoning is for room drawable space.
int BoundWidth = MAX_H -4;
int BoundHeight = MAX_V - 4;

//Set Max Room Width and Height
int MaxRoomWidth = 7;
int MaxRoomHeight = 8;
int MinRoomWidth = 3;
int MinRoomHeight = 2;

int offset = 2;
//Start Random Generation of Rooms
for(i = 0; i<Max_Rooms;i++){

	int x = rand()%BoundWidth + offset;
	int y = rand()%BoundHeight + offset;
	Room[i].x = x;
	Room[i].y = y;
	Room[i].w = rand()%MaxRoomWidth + MinRoomWidth; 
	Room[i].h = rand()%MaxRoomHeight + MinRoomHeight;
	Room[i].cx = x + Room[i].w/2;
	Room[i].cy = y + Room[i].h/2;
	 
	//Initially say the room isn't valid, to run through loop.
	Room[i].valid = false;

	//Debug Check Top Left, Center, Bottom Right;

	int attempt = 0;
	while(((Room[i].x + Room[i].w)>(MAX_H-2)) || 
		((Room[i].y + Room[i].h)>(MAX_V-2))||
		!Room[i].valid){

		
		int intersectioncount = 0;
		
		x = rand()%BoundWidth + offset;
		y = rand()%BoundHeight + offset;
		Room[i].x = x;
		Room[i].y = y;
		Room[i].w = rand()%MaxRoomWidth + MinRoomWidth; 
		Room[i].h = rand()%MaxRoomHeight + MinRoomHeight;
		Room[i].cx = x + Room[i].w/2;
		Room[i].cy = y + Room[i].h/2;	
		
		int k = 0;
		
		for(k = 0; k<i ; k++){
	
			if(checkRoomIntersection(Room[k],Room[i]) && k!=i){
				Room[i].valid = false;
				intersectioncount++;

				//Debug Check for Intersection and with what.
				//printf("attempt %d : Room Intersection with Room %d and %d and with %d rooms.\n",attempt,k,i,intersectioncount);


				attempt++;
			}
		}
		if(intersectioncount == 0){			
			Room[i].valid = true;
		}

		if(attempt > 1999){
			printf("Not enough space for new room, Max Rooms is now : %d.\n", i-1);
			Max_Rooms = i-1;
			break;		
		}
		
	}
	//Debug Check Room Locations
	/*
	int m;
	int n;
	for(m = Room[i].x; m<(Room[i].x+Room[i].w);m++){
		for(n = Room[i].y; n<(Room[i].y+Room[i].h);n++){
			dungeonmap[m][n] = '.';
		}
	}
	
	
	

	*/
	
}

//Draw Paths To Rooms
for(i = 0;i<(Max_Rooms-1);i++){
	//Copy the Current Hardness Map.
	int tempHardnessMap[MAX_H][MAX_V];
	memcpy(tempHardnessMap,rockhardnessmap,MAX_H*MAX_V*sizeof(int));
	
	
	
	int m, n, k;
	
	
	//For All Other Rooms, set hardness map as if it was a wall.
	for( k = 0; k<5;k++){
		if(k!=i && k!=(i+1)){
			for(m = (Room[k].x-1); m<(Room[k].x+Room[k].w+1);m++){
				for(n = (Room[k].y-1); n<(Room[k].y+Room[k].h+1);n++){
					if(m>0 && m<MAX_H && n>0 && n<MAX_V){
						tempHardnessMap[m][n] = 1000000;
					}
				}
			}
		}
	}
	//Debug Print New Hardness Map
	/*
	for(m = 0;m<MAX_V;m++){
		for(n=0;n<MAX_H;n++){
			if(tempHardnessMap[n][m] < 10){
			putchar(48+tempHardnessMap[n][m]);
			
			}
			else{
				putchar(' ');
			}	
		}
	putchar('\n');
	}
	putchar('\n');
	*/
	//Convert to 1D array for dijkstra's
	int i_start = Room[i].cx + Room[i].cy*MAX_H; 
	int i_end = Room[i+1].cx + Room[i+1].cy*MAX_H;
	//Calculate Shortest Path via Dijkstra's Algorithm.
	
	pList path;
	path = dikstras(tempHardnessMap, i_start, i_end);
	
	for(k = 0;k<path.size;k++){
		int x = path.List[k] % MAX_H;
		int y = path.List[k] / MAX_H;
		dungeonmap[x][y] = '#';
	}
	
	
	//printf("%d\n", count);
	
}



//Draw Rooms
for( i = 0; i<Max_Rooms;i++){
	int m;
	int n;
	for(m = Room[i].x; m<(Room[i].x+Room[i].w);m++){
		for(n = Room[i].y; n<(Room[i].y+Room[i].h);n++){
			dungeonmap[m][n] = '.';
		}
	}
	
	//Debug Check Room Number
	/*
	switch(i){
		case 0:
			dungeonmap[Room[i].x][Room[i].y] = '0';
		break;
		case 1:
			dungeonmap[Room[i].x][Room[i].y] = '1';
		break;
		case 2:
			dungeonmap[Room[i].x][Room[i].y] = '2';
		break;
		case 3:
			dungeonmap[Room[i].x][Room[i].y] = '3';
		break;
		case 4:
			dungeonmap[Room[i].x][Room[i].y] = '4';
		break;
	}
	*/
}



//Print Dungeon Map
for(j = 0; j<MAX_V;j++){ 
	for(i = 0; i<MAX_H;i++){
		if(dungeonmap[i][j] == 0){
			putchar(' ');
		}
		else{
			if(dungeonmap[i][j] == '.')
				printf("\x1b[30;43m");
			putchar(dungeonmap[i][j]);
			printf("\x1b[0m");
		}
	}
		putchar('\n');
}


return 0;
}


