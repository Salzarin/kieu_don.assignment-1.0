all:Dungeon
Dungeon:
	gcc -Wall -Werror -o Dungeon dungeon.c
clean:
	rm -f Dungeon *o *~
tar:
	tar cvfz ./../kieu_don.assignment-1.0.tar.gz ./../kieu_don.assignment-1.0/ --exclude .git
changelog:
	git log --date=local --pretty=format:"%ad, %h, %an, message: %s" > CHANGELOG
